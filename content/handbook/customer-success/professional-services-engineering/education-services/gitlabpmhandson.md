---
title: "GitLab Project Management - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab Project Management course."
---


## GitLab Project Management Lab Guides

| Lab Name | Lab Link |
|-----------|------------|
| Access the GitLab training environment | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1/) |
| Create an organizational structure in GitLab | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab2/) |
| Use GitLab planning tools | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab3/) |
| Create issues |  [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab4/) |
|  Organize and manage issues | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab5/) |
| Use a merge request to review and merge code | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab6/) |
| Create and customize issue boards |  [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab7/) |
| Create and manage a Kanban board | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab8/) |
| Create and manage a Scrum board |  [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab9/) |
|  Create and manage a Waterfall board | [Lab Link](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab10/) |

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

* [GitLab Project Management course description](https://about.gitlab.com/services/education/pm/)
* [GitLab Project Management Specialist certification details](https://about.gitlab.com/services/education/gitlab-project-management-associate/)

## Suggestions?

If you'd like to suggest changes to the *GitLab Project Management Hands-on Guide*, please submit them via merge request.
