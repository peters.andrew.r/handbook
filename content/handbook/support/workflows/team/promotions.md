---

title: Working on a promotion
category: Support Team
description: Process for submitting a promotions request
---



## Overview

This page supplements the [Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/)
and the [Support Engineer Career path](/handbook/support/support-engineer-career-path/#career-development-in-support) page. It documents workflows and processes specific to preparing your promotion document in the Customer Support department.

## Process

The general process for promotions is:

1. Support team member completes promotion document in collaboration with their
   manager.
   - Collaboration with direct peers can also be a great resource in moving a
     promotion document forward. A lot of team members find recognizing and
     praising their co-workers easier than doing so for themselves.
1. The manager circulates the promotion document for review and feedback
   amongst the manager's peers.
1. Support Directs review and calibrate all proposed promotions [twice per year](/handbook/people-group/promotions-transfers/#twice-per-year-promotion-calibration-process--timeline).
1. Approved promotions proceed through the rest of the [process](/handbook/people-group/promotions-transfers/#twice-per-year-promotion-calibration-process--timeline).

## Promotion Document

The promotion document template can be found in the
[Promotion document section of the Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/#promotion-document). This section of the handbook provides a lot of guidance on how to complete your document. Spend some time reading through it before you begin your document. You can refer to [completed Support team promotion documents](https://handbook.gitlab.com/handbook/support/workflows/team/promotions/#completed-promotion-documents) to see examples of what to include in each section of the document.

Business Results and Business Justifications section should closely align with
the expected competencies for the role as laid out in:

- The [GitLab Job Frameworks](/handbook/company/structure/#job-frameworks).
- The [Support Career Framework](/handbook/engineering/careers/matrix/support/).

### Feedback process for promotion to Staff+ roles

Promotion to a Staff role or higher requires completion of a Feedback process in addition to completing a promotion document.  This requires you to nominate 2-3 people who will provide feedback to the promotion approval process in the form of Start, Stop, Continue, and a general statement about their support for your promotion. Familiarise yourself with this process in the [Engineering Promotion Feedback](https://handbook.gitlab.com/handbook/people-group/promotions-transfers/#engineering-promotion-feedback-pilot) section of the handbook.

Work with your manager to determine who these people will be. It is likely that they will need to provide the feedback before your promotion document is complete, as both the promotion document and the feedback summary are typically due for submission at a similar time. The nominee input is based on what they have observed while working with you and is not dependent on your promotion document being in a final state.

## In-Progress Promotion Documents

Collaboration on promotion documents encourages transparency in the promotion process. It can
motivate and inspire others to build and strengthen their promotion documents. Internally
public promotion documents are part of GitLab's
[Promotion Philosophy](/handbook/people-group/promotions-transfers/#promotion-philosophy).
GitLab Support team members can link their internally
public promotion documents in the [In-Progress Promotion Documents](https://docs.google.com/document/d/1ECTvNgZD1j9BstQI08ei8B5KxzdO3fCOBMAFQrnDmQc/edit?usp=sharing) Google Doc available only to GitLab team members. Having an internally public in-progress promotion document is **optional**. If you are open to share your in-progress promotion document with the Support team you can also use the Slack channel: #support_team-chat.

If you want to list your document, decide what level of publicity you are comfortable with. Set the appropriate access for the GitLab group in the document's *Share* settings: You can keep it read-only (`Viewer`), allow others to comment and suggest edits (`Commenter`) – or fully open it to collaboration (`Editor`).

## Completed Promotion Documents

GitLab team members with the appropriate level of access can browse through the list of [Completed Promotions](https://docs.google.com/document/d/1A9hP1smFa0Z6upoljweg9KnxhwYPha1BZApIwRXICZY/edit?usp=sharing) documents.
